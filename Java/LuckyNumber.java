package com.mars.training;

import java.util.Scanner;

public class LuckyNumber {

	public static void main(String[] args) {
		final int luckyNumber = 7;
		boolean exhausted = true;

		for (int i = 0; i < 3; i++) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Guess the Lucky No(1 to 10): ");
			int input = scanner.nextInt();
			if (input == luckyNumber) {
				System.out.println("You found the lucky number in " + (i + 1) + " chances");
				exhausted = false;
				break;
			} else {
				System.out.println("Oops, try again!");
			}
		}
		System.out.println("Chances Exhaused");
		if (exhausted) {
			System.out.println("All 3 chances are exhausted");
		}
//		System.out.println("Completed ...");

	}
}
