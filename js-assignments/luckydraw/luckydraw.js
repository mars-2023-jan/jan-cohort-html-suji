const LUCKY_NUMBER = 7;
let attempt = 1;
let chance = 1;

function execute() {
  let input = document.getElementById("luckyno").value;

  console.log("Input = " + input);

  checkLuckyDraw(input);

  if (attempt++ % 3 == 0) {
    if (chance == 3) {
      msg = "Oops!! You are out of chances ...";
      document.getElementById("msg").innerHTML = msg;
      document.getElementById("confirm").style.display = "none";
    } else {
      msg =
        "You exhausetd your " + chance + " chance, Do you wanna continue ... ";
      document.getElementById("msg").innerHTML = msg;
      document.getElementById("confirm").style.display = "block";
      document.getElementById("checkbtn").style.display = "none";
    }
  }
}

function confirm() {
  document.getElementById("confirm").style.display = "none";
  document.getElementById("msg").innerHTML = "";
  document.getElementById("checkbtn").style.display = "block";
  chance++;
}

function checkLuckyDraw(input) {
  let msg = "";
  if (input == LUCKY_NUMBER) {
    msg =
      "Great!! You took " +
      attempt +
      " attempt in your " +
      chance +
      " chance to guess it right.";
  } else {
    msg = "Not really! Give it another try";
  }
  document.getElementById("msg").innerHTML = msg;
}
