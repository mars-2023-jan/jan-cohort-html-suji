let msg = "";

function addempdetails() {
  let empid = document.getElementById("getempid").value;
  let empname = document.getElementById("getempname").value;
  let designation = document.getElementById("getdesignation").value;
  let workhours = document.getElementById("getworkhours").value;

  let employee = {};
  employee.empid = empid;
  employee.empname = empname;
  employee.designation = designation;
  employee.workhours = workhours;

  if (employee.designation.toLowerCase() == "Manager".toLowerCase()) {
    employee.salary = employee.workhours * 50;
  }
  if (employee.designation.toLowerCase() == "Consultant".toLowerCase()) {
    employee.salary = employee.workhours * 30;
  }

  if (employee.designation.toLowerCase() == "Trainee".toLowerCase()) {
    employee.salary = employee.workhours * 20;
  }

  let employeesArr = localStorage.getItem("employeesArr") || [];
  if (employeesArr.length != 0) {
    employeesArr = JSON.parse(employeesArr);
  }

  employeesArr.push(employee);
  localStorage.setItem("employeesArr", JSON.stringify(employeesArr));

  alert("Your Information is Saved!!");
  // console.log(localStorage.getItem("employeesArr"));

  // msg =
  //   employee.empname +
  //   " is a " +
  //   employee.designation +
  //   "  and gets $" +
  //   employee.salary;

  // document.getElementById("display").innerHTML = msg;
}

function resetdata() {
  document.getElementById("getempid").value = "";
  document.getElementById("getempname").value = "";
  document.getElementById("getdesignation").value = "";
  document.getElementById("getworkhours").value = "";
}

function onSubmit() {
  let employeesArr = JSON.parse(localStorage.getItem("employeesArr"));
  console.log("Local Storage Data = " + JSON.stringify(employeesArr));

  
    let highestSalaryEmployee = employeesArr[0];
    for (let i = 0; i < employeesArr.length; i++) {
      if (highestSalaryEmployee.salary < employeesArr[i].salary) {
        highestSalaryEmployee = employeesArr[i];
      }
    }
    msg =
      highestSalaryEmployee.empname +
      " is a " +
      highestSalaryEmployee.designation +
      "  and gets higher salary of $" +
      highestSalaryEmployee.salary;

    document.getElementById("display").innerHTML = msg;
  } 
