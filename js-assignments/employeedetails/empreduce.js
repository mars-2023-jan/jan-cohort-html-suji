let employeeArr = [];
let msg = "";

function addempdetails() {
  let empid = document.getElementById("getempid").value;
  let empname = document.getElementById("getempname").value;
  let designation = document.getElementById("getdesignation").value;
  let workhours = document.getElementById("getworkhours").value;

  let employee = {};
  employee.empid = empid;
  employee.empname = empname;
  employee.designation = designation;
  employee.workhours = workhours;

  if (employee.designation.toLowerCase() == "Manager".toLowerCase()) {
    employee.salary = employee.workhours * 50;
  }
  if (employee.designation.toLowerCase() == "Consultant".toLowerCase()) {
    employee.salary = employee.workhours * 30;
  }

  if (employee.designation.toLowerCase() == "Trainee".toLowerCase()) {
    employee.salary = employee.workhours * 20;
  }

  // let employeesArr = localStorage.getItem("employeesArr") || [];
  // if (employeesArr.length != 0) {
  //   employeesArr = JSON.parse(employeesArr);
  // }

  // employeesArr.push(employee);
  // localStorage.setItem("employeesArr", JSON.stringify(employeesArr));

  let salary = employee.salary;

  alert("Your Information is Saved!!");

  employeeArr.push(employee);

  console.log(employee);
}

function resetData() {
  document.getElementById("getempid").value = "";
  document.getElementById("getempname").value = "";
  document.getElementById("getdesignation").value = "";
  document.getElementById("getworkhours").value = "";
}

// function gethighsalary(highestSalaryEmployee, employee) {
//   if (highestSalaryEmployee.salary < employee.salary) {
//     highestSalaryEmployee = employee;
//   }
//   return highestSalaryEmployee;
// }

function onSubmit() {
  const result = employeeArr.reduce((highestSalaryEmployee, employee) => {
    if (highestSalaryEmployee.salary < employee.salary) {
      highestSalaryEmployee = employee;
    }
    return highestSalaryEmployee;
  });

  msg =
    result.empname +
    " is a " +
    result.designation +
    "  and gets higher salary of $" +
    result.salary;

  document.getElementById("display").innerHTML = msg;
}
