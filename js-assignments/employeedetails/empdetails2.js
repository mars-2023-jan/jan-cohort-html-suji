let msg = "";
let employeesArr = [];

function addempdetails() {
  let empid = document.getElementById("getempid").value;
  let empname = document.getElementById("getempname").value;
  let designation = document.getElementById("getdesignation").value;
  let workhours = document.getElementById("getworkhours").value;

  if (designation.toLowerCase() == "Manager".toLowerCase()) {
    salary = workhours * 50;
  }

  if (designation.toLowerCase() == "Consultant".toLowerCase()) {
    salary = workhours * 30;
  }

  if (designation.toLowerCase() == "Trainee".toLowerCase()) {
    salary = workhours * 20;
  }

  let employee = new Employee(empid, empname, designation, workhours, salary);

  console.log("Employee=" + employee);

  // let employeesArr = localStorage.getItem("employeesArr") || [];
  // if (employeesArr.length != 0) {
  //   employeesArr = JSON.parse(employeesArr);
  // }

  employeesArr.push(employee);
  // localStorage.setItem("employeesArr", JSON.stringify(employeesArr));

  alert("Your Information is Saved!!");
  // console.log(localStorage.getItem("employeesArr"));

  // msg =
  //   employee.empname +
  //   " is a " +
  //   employee.designation +
  //   "  and gets $" +
  //   employee.salary;

  // document.getElementById("display").innerHTML = msg;
}

function resetData() {
  document.getElementById("getempid").value = "";
  document.getElementById("getempname").value = "";
  document.getElementById("getdesignation").value = "";
  document.getElementById("getworkhours").value = "";
}

function onSubmit() {
  // let employeesArr = JSON.parse(localStorage.getItem("employeesArr"));
  // console.log("Local Storage Data = " + JSON.stringify(employeesArr));

  let highestSalaryEmployee = employeesArr[0];
  for (let i = 0; i < employeesArr.length; i++) {
    if (highestSalaryEmployee.salary < employeesArr[i].salary) {
      highestSalaryEmployee = employeesArr[i];
    }
  }
  msg =
    highestSalaryEmployee.empname +
    " is a " +
    highestSalaryEmployee.designation +
    "  and gets higher salary of $" +
    highestSalaryEmployee.salary;

  document.getElementById("display").innerHTML = msg;
}

class Employee {
  #empidValue;
  #empnameValue;
  #designationValue;
  #workhoursValue;
  #salaryValue;

  constructor(empid, empname, designation, workhours, salary) {
    this.empid = empid;
    this.empname = empname;
    this.designation = designation;
    this.workhours = workhours;
    this.salary = salary;
  }

  get empid() {
    return this.#empidValue;
  }

  set empid(_empid) {
    this.#empidValue = _empid;
  }

  get empname() {
    return this.#empnameValue;
  }

  set empname(_empname) {
    this.#empnameValue = _empname;
  }

  get designation() {
    return this.#designationValue;
  }

  set designation(_designation) {
    this.#designationValue = _designation;
  }

  get workhours() {
    return this.#workhoursValue;
  }

  set workhours(_workhours) {
    this.#workhoursValue = _workhours;
  }

  get salary() {
    return this.#salaryValue;
  }

  set salary(_salary) {
    this.#salaryValue = _salary;
  }
}
