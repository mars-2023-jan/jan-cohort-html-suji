function clearScreen() {
  document.getElementById("result").value = "";
}

let displayBox = document.getElementById("result");

function display(value) {
  displayBox.value += value;
}

function backspace() {
  displayBox.value = displayBox.value.slice(0, -1);
}

function calculate() {
  p = displayBox.value;
  var q = eval(p);
  displayBox.value = q;
}
