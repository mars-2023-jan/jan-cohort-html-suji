let count = 1;

function getInputNumber() {
   let input = document.getElementById("luckyno").value;
   const LUCKY_NUMBER = 7;

  let message = "";
  if (input == LUCKY_NUMBER) {
    message = "Great!! You took " + count + " attempts to get it right!";
  } else {
    message = "Not really! Give it another try";
    count++;
  }
  console.log(message);
  document.getElementById("message").innerHTML = message;
}