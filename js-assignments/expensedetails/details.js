function showDetailsTable() {
  let data = JSON.parse(localStorage.getItem("data"));
  console.log("Local Storage Data = " + JSON.stringify(data));

  let table = document.getElementById("my-table");

  for (let i = 0; i < data.length; i++) {
    let row = "";
    row += `<tr>`;
    row += `<td>${data[i].date}</td>`;
    row += `<td>${data[i].mortage}</td>`;
    row += `<td>${data[i].gas}</td>`;
    row += `<td>${data[i].childcare}</td>`;
    row += `<td>${data[i].insurance}</td>`;
    row += `<td>${data[i].selectedRadioButton}</td>`;
    row += `<td>${data[i].misc}</td>`;
    row += `<td>${data[i].checkedValue}</td>`;
    row += `</tr>`;
    table.innerHTML += row;
  }
}
