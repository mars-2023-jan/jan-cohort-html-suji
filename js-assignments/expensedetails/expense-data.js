function getdata() {
  let date = document.getElementById("getdate").value;
  let mortage = document.getElementById("getmortage").value;
  let gas = document.getElementById("getgas").value;
  let childcare = document.getElementById("getchildcare").value;
  let insurance = document.getElementById("getinsurance").value;
  let misc = document.getElementById("getmisc").value;

  //check button

  var checkedValue = [];
  var inputElements = document.getElementsByClassName("check");
  for (var i = 0; inputElements[i]; ++i) {
    if (inputElements[i].checked) {
      checkedValue.push(inputElements[i].value);
    }
  }

  //radiobutton
  let selectedRadioButton = document.querySelector(
    'input[name="monthlyoryearly"]:checked'
  ).value;

  let json = {};

  json.date = date;
  json.mortage = mortage;
  json.gas = gas;
  json.childcare = childcare;
  json.insurance = insurance;
  json.selectedRadioButton = selectedRadioButton;
  json.misc = misc;
  json.checkedValue = checkedValue.toString();

  let tableItems = localStorage.getItem("data") || [];
  if (tableItems.length != 0) {
    tableItems = JSON.parse(tableItems);
  }

  tableItems.push(json);
  localStorage.setItem("data", JSON.stringify(tableItems));
  console.log(localStorage.getItem("data"));
}

function resetdata() {
  document.getElementById("getdate").value = "";
  document.getElementById("getmortage").value = "";
  document.getElementById("getgas").value = "";
  document.getElementById("getchildcare").value = "";
  document.getElementById("getinsurance").value = "";
  document.getElementById("getmisc").value = "";
}
