/*Assignment 3*/
/*create a procedure to insert/update/delete data in to product and order1 table by handling errors*/

/*PRODUCT*/

DELIMITER $$

CREATE PROCEDURE `product_modify`(IN `action` VARCHAR(10), 
								  IN `prod_id` varchar(6), 
                                  IN `prod_name` VARCHAR(10), 
                                  IN `prod_desc` VARCHAR(20), 
                                  IN `prod_price` DECIMAL(8,2)
                                  )
BEGIN
    IF action = 'insert' THEN
        INSERT INTO product (
						product_id,
						product_name, 
                        product_desc, 
                        price)
		VALUES (
						prod_id,
						prod_name, 
                        prod_desc, 
                        prod_price);
                        
    ELSEIF action = 'update' THEN
        UPDATE product SET product_name = prod_name, 
						   product_desc = prod_desc, 
                           price = prod_price 
					WHERE product_id = prod_id;
    ELSEIF action = 'delete' THEN
        DELETE FROM product WHERE product_id = prod_id;
    END IF;
    

END $$

DELIMITER ;

select * from product;

call product_modify('insert', 'EL106', 'LENOVO', 'DELL', 510);

SET SQL_SAFE_UPDATES = 0;

CALL product_modify('update', 'KZ104', 'TOYTRAIN', 'LEGO', 250);

SET autocommit=0;
rollback;

CALL `product_modify`('delete','EL104', NULL, NULL, NULL);

=================================================================================================================

/*ORDER1*/

DELIMITER $$

CREATE PROCEDURE `order_modify`(IN `action`   VARCHAR(10), 
								IN `order_id`   VARCHAR(6), 
                                IN `order_date` VARCHAR(10), 
                                IN `order_type` VARCHAR(10),
                                IN `pr_id`  VARCHAR(6)
                                  )
BEGIN
    IF action = 'insert' THEN
        INSERT INTO order1 (
						ord_id,
						ord_date, 
                        ord_type, 
                        prod_id)
		VALUES (
						order_id,
						order_date, 
                        order_type, 
                        pr_id);
                        
   ELSEIF action = 'update' THEN
        UPDATE order1 SET ord_date = order_date, 
						  ord_type = order_type, 
                          prod_id = pr_id 
					WHERE ord_id = order_id;
    ELSEIF action = 'delete' THEN
        DELETE FROM order1 WHERE ord_id = order_id;
    END IF;
END $$

DELIMITER ;

select * from order1;

SET FOREIGN_KEY_CHECKS=0;

CALL order_modify('insert', 'ORD106', '2022-01-11', 'NEW', 'EL106');

CALL order_modify('update', 'ORD103', '2022-10-09', 'NEW', 'EL103');

CALL order_modify('delete','ORD101', NULL, NULL, NULL);